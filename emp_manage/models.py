from django.db import models
from django.conf import settings

"""
Since we do not need login/authentication for employees,
using custom Employee model rather than using django.contrib.auth.models.User
"""

class Designation(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Employee(models.Model):
    employee_number = models.CharField(max_length=255,unique=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=254,unique=True)
    address = models.CharField(max_length=500)
    photo = models.ImageField(upload_to='emp_photos',null=True,blank=True,help_text=f'Maximum file size is {settings.IMAGE_MAX_UPLOAD_SIZE} MB')
    designation = models.ForeignKey(Designation,on_delete=models.CASCADE)