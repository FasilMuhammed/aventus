from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
def getattribute(obj,key):
    try:
        if str(key) in obj:
            return obj[str(key)]
    except:
        if hasattr(obj,str(key)):
            return getattr(obj,str(key)) if getattr(obj,str(key)) else '-'
    return '-'

@register.simple_tag
def getaction(obj,key):
    actions = obj.get(key,[])
    html = ''
    for action in actions:
        html += f'<a '
        for attr_key,attr_val in action.items():
            if attr_key != "label":
                html += f'{attr_key}="{attr_val}"'
        html += f'>{action["label"]}</a>'
    return mark_safe(html)