from rest_framework.serializers import ModelSerializer

from emp_manage.models import Employee

class EmployeeSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = ['employee_number','name','email','address','photo','designation']
        depth = 1