from rest_framework.viewsets import ModelViewSet

from emp_manage.models import Employee

from .serializers import EmployeeSerializer

class EmployeeViewSet(ModelViewSet):
    model = Employee
    serializer_class = EmployeeSerializer

    def get_queryset(self):
        return self.model.objects.all()