from django.forms import ModelForm,ImageField
from django.conf import settings

from .models import Employee

class EmployeeAddForm(ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'
    
    def clean_photo(self):
        photo = self.cleaned_data['photo']
        if photo:
            size = photo.size
            max_size = (settings.IMAGE_MAX_UPLOAD_SIZE*1024*1024)
            if size > max_size:
                self.add_error('photo',f'Maximum file size is {settings.IMAGE_MAX_UPLOAD_SIZE} MB')
        return photo