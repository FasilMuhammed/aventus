from django.urls import path
from . import views as emp

app_name = 'emp_manage'

urlpatterns = [
    path('add',emp.EmployeeAddView.as_view(),name='add_employee'),
    path('list',emp.EmployeeListView.as_view(),name='list_employee'),
    path('edit/<int:pk>',emp.EmployeeEditView.as_view(),name='edit_employee'),
    path('delete/<int:pk>',emp.EmployeeDeleteView.as_view(),name='delete_employee')
]