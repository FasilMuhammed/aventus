from copy import deepcopy

from django.shortcuts import render,get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic.edit import FormView,DeleteView
from django.views.generic.list import ListView
from django.db.models import Count

from .forms import EmployeeAddForm
from .models import Designation, Employee

class HomeView(View):
    template_name = 'emp_manage/dashboard.html'
    model = Employee

    def get_queryset(self):
        qs = self.model.objects.values('designation__name').annotate(dcount=Count('designation__name')).order_by('designation__name')
        all_designations = deepcopy(Designation.objects.values_list('name',flat=True))
        count_data = {}
        for items in qs:
            count_data[items['designation__name']] = items['dcount']

        for designation in all_designations:
            if designation not in count_data:
                count_data[designation] = 0
        return count_data

    def get(self,request,*args,**kwargs):
        context = {}
        context['title'] = context['section_title'] = context['page_title'] = 'Dashboard'
        context['object_list'] = self.get_queryset()
        return render(request,self.template_name,context)

class EmployeeAddView(FormView):
    template_name = 'common_form.html'
    form_class = EmployeeAddForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].buttons = [
            {'label':'Submit','type':'submit','class':'btn btn-primary float-right'}
        ]
        context['page_title'] = 'Add Employees'
        context['page_header'] = {
            'icon' : '<i class="nav-icon fas fa-user-plus"></i>',
            'title' : 'Add Employees'
        }
        return context
    
    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
    
    def get_success_url(self):
        return reverse('emp_manage:list_employee')

class EmployeeListView(ListView):
    model = Employee
    template_name = 'data_table.html'
    ordering = '-id'

    headers = {
        'employee_number' : 'Employee Number',
        'name' : 'Name',
        'email' : 'Email',
        'address' : 'Address',
        'designation__name' : 'Designation',
        'actions' : 'Actions'
    }
    field_list = [
        'id',
        'employee_number',
        'name',
        'email',
        'address',
        'designation__name'
    ]

    def get_queryset(self):
        queryset = deepcopy(super().get_queryset().values(*self.field_list))
        for qs in queryset:
            qs['actions'] = [
                {'label' : 'Edit', 'href' : reverse('emp_manage:edit_employee',kwargs={'pk':qs['id']}), 'class' : 'btn text-primary ml-1'},
                {'label' : 'Delete', 'href' : reverse('emp_manage:delete_employee',kwargs={'pk':qs['id']}), 'class' : 'btn text-danger ml-1 emp-delete-btn'},
            ]
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['headers'] = self.headers
        context['page_title'] = 'Employee List'
        context['page_header'] = {
            'icon' : '<i class="nav-icon fas fa-table"></i>',
            'title' : 'Employee List'
        }
        context['value_as_action'] = ['actions']
        return context

class EmployeeEditView(EmployeeAddView):
    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['instance'] = get_object_or_404(Employee,id=self.kwargs['pk'])
        return form_kwargs
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].buttons = [
            {'label':'Submit','type':'submit','class':'btn btn-primary float-right'}
        ]
        context['page_title'] = 'Edit Employee'
        context['page_header'] = {
            'icon' : '<i class="nav-icon fas fa-edit"></i>',
            'title' : 'Edit Employee'
        }
        context['breadcrumbs'] = {
            reverse('emp_manage:list_employee') : 'Employee List'
        }
        return context

class EmployeeDeleteView(DeleteView):
    model = Employee
    template_name = 'emp_manage/delete_employee.html'
    
    def get_success_url(self):
        return reverse('emp_manage:list_employee')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Delete Employee'
        context['page_header'] = {
            'icon' : '<i class="nav-icon fas fa-trash"></i>',
            'title' : 'Delete Employee'
        }
        context['breadcrumbs'] = {
            reverse('emp_manage:list_employee') : 'Employee List'
        }
        return context