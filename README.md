# Aventus Employee Management


## Getting started


# Install Project

        step 1 : git clone https://gitlab.com/FasilMuhammed/aventus

        step 2 : create a virtual environment by using "venv" or "pipenv".

        step 3 : install the packages - run "pip install -r requirements.txt"
        
        step 4 : run the application - python3 manage.py runserver


# API Details

        available apis : 

            1. Employee List API

                url : http://localhost:8000/api/v1/employees/


# DB used : Sqlite3